package com.epam;

import java.util.Scanner;

/**
 * Application is the main class!
 *
 * @author Andrii Makarchuk
 *
 */
public final class Application {

    /**
     * Enter point of program!
     *
     * @param args input array
     */
    public static void main(final String[] args) {
        Scanner scanner = new Scanner(System.in);
        Counter counter;
        int startValue;
        int endValue;
        int sizeOfSet;
        System.out.print("Enter start value: ");
        startValue = scanner.nextInt();
        System.out.print("End start value: ");
        endValue = scanner.nextInt();
        System.out.print("Enter amount of fibonacci set: ");
        sizeOfSet = scanner.nextInt();
        counter = new Counter(startValue, endValue, sizeOfSet);
        counter.printOddAndEvenNumbersAndSums();
        counter.maxOddAndEvenFibonacciNumbers();
    }

    /**
     * Default constructor!
     */
    private Application() {
    }
}

