package com.epam;

/**
 * Counter is the class of main calculation in program!
 *
 * @author Andrii Makarchuk
 *
 */

public class Counter {

    /**
     * The static field of 100 for transformation to percents!
     */
    private static final double HUNDRED_PERCENT = 100.0;

    /**
     * The static field of 50!
     */
    private static final double FIFTY_PERCENT = 50.0;

    /**
     * The static field of 0!
     */
    private static final double ZERO_PERCENT = 0.0;

    /**
     * The static field of 0!
     */
    private static final int ZERO_NUMBER = 0;

    /**
     * The static field of 1!
     */
    private static final int ONE_NUMBER = 1;

    /**
     * Start of numbers interval!
     */
    private int startValue;

    /**
     * End of numbers interval!
     */
    private int endValue;

    /**
     * Amount numbers of Fibonacci sequence!
     */
    private int sizeOfFibonacciSet;

    /**
     * Constructor with all input parameters!
     *
     * @param inputStartValue         start value of interval
     * @param inputEndValue           last value of interval
     * @param inputSizeOfFibonacciSet amount numbers of Fibonacci sequence
     */
    Counter(final int inputStartValue, final int inputEndValue,
            final int inputSizeOfFibonacciSet) {
        this.startValue = inputStartValue;
        this.endValue = inputEndValue;
        this.sizeOfFibonacciSet = inputSizeOfFibonacciSet;
    }

    /**
     * Default constructor!
     */
    Counter() {
    }

    /**
     * Method which generate and output sequences
     * of ood and even values and their sums!
     */
    final void printOddAndEvenNumbersAndSums() {
        int oddSum = 0;
        int evenSum = 0;
        for (int i = startValue; i <= endValue; i++) {
            if (i % 2 == 1) {
                System.out.println(i);
                oddSum += i;
            }
        }
        for (int i = endValue; i >= startValue; i--) {
            if (i % 2 == 0) {
                System.out.println(i);
                evenSum += i;
            }
        }
        System.out.println("Odd sum numbers of interval: " + oddSum);
        System.out.println("Even sum numbers of interval: " + evenSum);
    }

    /**
     * Method which generate and output max odd and even num of
     * Fibonacci sequence and frequency of even and odd numbers!
     */

    final void maxOddAndEvenFibonacciNumbers() {
        int maxOdd = ONE_NUMBER;
        int maxEven = ZERO_NUMBER;
        double evenPercentage = ONE_NUMBER;
        double oddPercentage = ONE_NUMBER;
        if (sizeOfFibonacciSet <= 0) {
            outValues(ZERO_NUMBER, ZERO_NUMBER, ZERO_PERCENT, ZERO_PERCENT);
        } else if (sizeOfFibonacciSet == 1) {
            outValues(ZERO_NUMBER, ZERO_NUMBER, ZERO_PERCENT, HUNDRED_PERCENT);
        } else if (sizeOfFibonacciSet == 2) {
            outValues(ONE_NUMBER, ZERO_NUMBER, FIFTY_PERCENT, FIFTY_PERCENT);
        } else {
            int currentMaxValue = 1;
            int prevValue = 0;
            int supportValue;
            for (int i = 0; i < sizeOfFibonacciSet - 2; i++) {
                supportValue = currentMaxValue;
                currentMaxValue += prevValue;
                prevValue = supportValue;
                if (currentMaxValue % 2 == 1) {
                    maxOdd = currentMaxValue;
                    oddPercentage++;
                } else {
                    maxEven = currentMaxValue;
                    evenPercentage++;
                }
            }
            evenPercentage = evenPercentage / sizeOfFibonacciSet;
            evenPercentage *= HUNDRED_PERCENT;
            oddPercentage = oddPercentage / sizeOfFibonacciSet;
            oddPercentage *= HUNDRED_PERCENT;
            outValues(maxOdd, maxEven, oddPercentage, evenPercentage);
        }
    }

    /**
     * Method which output sequences of ood and even values and their sums!
     *
     * @param maxOdd input max odd value
     * @param maxEven input max even value
     * @param oddPercentage input ood percentage
     * @param evenPercentage input even percentage
     */
    private void outValues(final int maxOdd, final int maxEven,
                           final double oddPercentage,
                           final double evenPercentage) {
        System.out.println("Max odd number is: " + maxOdd);
        System.out.println("Max even number is: " + maxEven);
        System.out.println("Percentage of odd numbers is: " + oddPercentage);
        System.out.println("Percentage of even numbers is: " + evenPercentage);
    }

    /**
     * Getter for startValue!
     *
     * @return start value of sequence
     */
    final int getStartValue() {
        return startValue;
    }

    /**
     * Getter for endValue!
     *
     * @return last value of sequence
     */
    final int getEndValue() {
        return endValue;
    }

    /**
     * Getter for sizeOfFibonacciSet!
     *
     * @return size of Fibonacci sequence
     */
    final int getSizeOfFibonacciSet() {
        return sizeOfFibonacciSet;
    }

    /**
     * Setter for startValue!
     *
     * @param inputStartValue start value of sequence
     */
    final void setStartValue(final int inputStartValue) {
        this.startValue = inputStartValue;
    }

    /**
     * Setter for endValue!
     *
     * @param inputEndValue last value of sequence
     */
    final void setEndValue(final int inputEndValue) {
        this.endValue = inputEndValue;
    }

    /**
     * Setter for sizeOfFibonacciSet!
     *
     * @param inputSizeOfFibonacciSet size of Fibonacci sequence
     */
    final void setSizeOfFibonacciSet(final int inputSizeOfFibonacciSet) {
        this.sizeOfFibonacciSet = inputSizeOfFibonacciSet;
    }
}
